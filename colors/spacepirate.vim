" SpacePirate.vim -- Vim color scheme.
" Author:      Nadim Edde Gomez (nadim@eddegomez.org)
" Webpage:     https://gitlab.com/Nequo/spacepirate/
" Description: Personal rewrite with xterm colors of 'vim-code-dark'
" Last Change: 2021-01-25

hi clear
if exists("syntax_on")
  syntax reset
endif

let colors_name = "spacepirate"

if ($TERM =~ '256' || &t_Co >= 256) || has("gui_running")
    hi Delimiter ctermbg=NONE ctermfg=250 cterm=NONE guibg=NONE guifg=#bcbcbc gui=NONE
    hi Normal ctermbg=234 ctermfg=250 cterm=NONE guibg=#1c1c1c guifg=#bcbcbc gui=NONE
    hi NonText ctermbg=NONE ctermfg=240 cterm=NONE guibg=NONE guifg=#585858 gui=NONE
    hi EndOfBuffer ctermbg=NONE ctermfg=240 cterm=NONE guibg=NONE guifg=#585858 gui=NONE
    hi LineNr ctermbg=233 ctermfg=238 cterm=NONE guibg=#121212 guifg=#444444 gui=NONE
    hi FoldColumn ctermbg=233 ctermfg=242 cterm=NONE guibg=#121212 guifg=#6c6c6c gui=NONE
    hi Folded ctermbg=233 ctermfg=242 cterm=NONE guibg=#121212 guifg=#6c6c6c gui=NONE
    hi MatchParen ctermbg=238 ctermfg=186 cterm=NONE guibg=#444444 guifg=#d7d787 gui=NONE
    hi SignColumn ctermbg=233 ctermfg=242 cterm=NONE guibg=#121212 guifg=#6c6c6c gui=NONE
    hi Comment ctermbg=NONE ctermfg=65 cterm=NONE guibg=NONE guifg=#5f875f gui=NONE
    hi Conceal ctermbg=NONE ctermfg=250 cterm=NONE guibg=NONE guifg=#bcbcbc gui=NONE
    hi Constant ctermbg=NONE ctermfg=173 cterm=NONE guibg=NONE guifg=#d7875f gui=NONE
    hi Error ctermbg=NONE ctermfg=167 cterm=reverse guibg=NONE guifg=#d75f5f gui=reverse
    hi Identifier ctermbg=NONE ctermfg=117 cterm=NONE guibg=NONE guifg=#87d7ff gui=NONE
    hi Ignore ctermbg=NONE ctermfg=NONE cterm=NONE guibg=NONE guifg=NONE gui=NONE
    hi PreProc ctermbg=NONE ctermfg=175 cterm=NONE guibg=NONE guifg=#d787af gui=NONE
    hi Special ctermbg=NONE ctermfg=179 cterm=NONE guibg=NONE guifg=#d7af5f gui=NONE
    hi Statement ctermbg=NONE ctermfg=74 cterm=NONE guibg=NONE guifg=#5fafd7 gui=NONE
    hi String ctermbg=NONE ctermfg=173 cterm=NONE guibg=NONE guifg=#d7875f gui=NONE
    hi Todo ctermbg=NONE ctermfg=179 cterm=reverse guibg=NONE guifg=#d7af5f gui=reverse
    hi Type ctermbg=NONE ctermfg=103 cterm=NONE guibg=NONE guifg=#8787AF gui=NONE
    hi Underlined ctermbg=NONE ctermfg=73 cterm=underline guibg=NONE guifg=#5FAFAF gui=underline
    hi Pmenu ctermbg=236 ctermfg=250 cterm=NONE guibg=#303030 guifg=#bcbcbc gui=NONE
    hi PmenuSbar ctermbg=240 ctermfg=NONE cterm=NONE guibg=#585858 guifg=NONE gui=NONE
    hi PmenuSel ctermbg=24 ctermfg=253 cterm=NONE guibg=#005f87 guifg=#DADADA gui=NONE
    hi PmenuThumb ctermbg=73 ctermfg=73 cterm=NONE guibg=#5FAFAF guifg=#5FAFAF gui=NONE
    hi ErrorMsg ctermbg=235 ctermfg=167 cterm=reverse guibg=#262626 guifg=#d75f5f gui=reverse
    hi ModeMsg ctermbg=235 ctermfg=65 cterm=reverse guibg=#262626 guifg=#5f875f gui=reverse
    hi MoreMsg ctermbg=NONE ctermfg=73 cterm=NONE guibg=NONE guifg=#5FAFAF gui=NONE
    hi Question ctermbg=NONE ctermfg=65 cterm=NONE guibg=NONE guifg=#5f875f gui=NONE
    hi WarningMsg ctermbg=NONE ctermfg=167 cterm=NONE guibg=NONE guifg=#d75f5f gui=NONE
    hi TabLine ctermbg=238 ctermfg=74 cterm=NONE guibg=#444444 guifg=#5fafd7 gui=NONE
    hi TabLineFill ctermbg=238 ctermfg=238 cterm=NONE guibg=#444444 guifg=#444444 gui=NONE
    hi TabLineSel ctermbg=74 ctermfg=235 cterm=NONE guibg=#5fafd7 guifg=#262626 gui=NONE
    hi ToolbarLine ctermbg=233 ctermfg=NONE cterm=NONE guibg=#121212 guifg=NONE gui=NONE
    hi ToolbarButton ctermbg=240 ctermfg=250 cterm=NONE guibg=#585858 guifg=#bcbcbc gui=NONE
    hi Cursor ctermbg=242 ctermfg=NONE cterm=NONE guibg=#6c6c6c guifg=NONE gui=NONE
    hi CursorColumn ctermbg=235 ctermfg=NONE cterm=NONE guibg=#262626 guifg=NONE gui=NONE
    hi CursorLineNr ctermbg=235 ctermfg=73 cterm=NONE guibg=#262626 guifg=#5FAFAF gui=NONE
    hi CursorLine ctermbg=235 ctermfg=NONE cterm=NONE guibg=#262626 guifg=NONE gui=NONE
    hi helpLeadBlank ctermbg=NONE ctermfg=NONE cterm=NONE guibg=NONE guifg=NONE gui=NONE
    hi helpNormal ctermbg=NONE ctermfg=NONE cterm=NONE guibg=NONE guifg=NONE gui=NONE
    hi StatusLine ctermbg=236 ctermfg=253 cterm=NONE guibg=#303030 guifg=#DADADA gui=NONE
    hi StatusLineNC ctermbg=238 ctermfg=74 cterm=NONE guibg=#444444 guifg=#5fafd7 gui=NONE
    hi StatusLineTerm ctermbg=74 ctermfg=235 cterm=NONE guibg=#5fafd7 guifg=#262626 gui=NONE
    hi StatusLineTermNC ctermbg=238 ctermfg=74 cterm=NONE guibg=#444444 guifg=#5fafd7 gui=NONE
    hi Visual ctermbg=253 ctermfg=24 cterm=reverse guibg=#DADADA guifg=#005f87 gui=reverse
    hi VisualNOS ctermbg=NONE ctermfg=NONE cterm=underline guibg=NONE guifg=NONE gui=underline
    hi VertSplit ctermbg=238 ctermfg=238 cterm=NONE guibg=#444444 guifg=#444444 gui=NONE
    hi WildMenu ctermbg=24 ctermfg=253 cterm=NONE guibg=#005f87 guifg=#DADADA gui=NONE
    hi Function ctermbg=NONE ctermfg=186 cterm=NONE guibg=NONE guifg=#d7d787 gui=NONE
    hi SpecialKey ctermbg=NONE ctermfg=240 cterm=NONE guibg=NONE guifg=#585858 gui=NONE
    hi Title ctermbg=NONE ctermfg=253 cterm=NONE guibg=NONE guifg=#DADADA gui=NONE
    hi DiffAdd ctermbg=235 ctermfg=65 cterm=reverse guibg=#262626 guifg=#5f875f gui=reverse
    hi DiffChange ctermbg=235 ctermfg=103 cterm=reverse guibg=#262626 guifg=#8787AF gui=reverse
    hi DiffDelete ctermbg=235 ctermfg=167 cterm=reverse guibg=#262626 guifg=#d75f5f gui=reverse
    hi DiffText ctermbg=235 ctermfg=173 cterm=reverse guibg=#262626 guifg=#d7875f gui=reverse
    hi IncSearch ctermbg=186 ctermfg=235 cterm=NONE guibg=#d7d787 guifg=#262626 gui=NONE
    hi Search ctermbg=24 ctermfg=253 cterm=NONE guibg=#005f87 guifg=#DADADA gui=NONE
    hi Directory ctermbg=NONE ctermfg=73 cterm=NONE guibg=NONE guifg=#5FAFAF gui=NONE
    hi debugPC ctermbg=74 ctermfg=NONE cterm=NONE guibg=#5fafd7 guifg=NONE gui=NONE
    hi debugBreakpoint ctermbg=167 ctermfg=NONE cterm=NONE guibg=#d75f5f guifg=NONE gui=NONE
    hi SpellBad ctermbg=NONE ctermfg=167 cterm=undercurl guibg=NONE guifg=#d75f5f gui=undercurl guisp=#d75f5f
    hi SpellCap ctermbg=NONE ctermfg=73 cterm=undercurl guibg=NONE guifg=#5FAFAF gui=undercurl guisp=#5FAFAF
    hi SpellLocal ctermbg=NONE ctermfg=65 cterm=undercurl guibg=NONE guifg=#5f875f gui=undercurl guisp=#5f875f
    hi SpellRare ctermbg=NONE ctermfg=173 cterm=undercurl guibg=NONE guifg=#d7875f gui=undercurl guisp=#d7875f
    hi ColorColumn ctermbg=233 ctermfg=NONE cterm=NONE guibg=#121212 guifg=NONE gui=NONE
    hi DiffAdd ctermbg=233 ctermfg=65 cterm=NONE guibg=#121212 guifg=#5f875f gui=NONE
    hi DiffChange ctermbg=233 ctermfg=103 cterm=NONE guibg=#121212 guifg=#8787AF gui=NONE
    hi DiffDelete ctermbg=233 ctermfg=167 cterm=NONE guibg=#121212 guifg=#d75f5f gui=NONE
    hi goRepeat ctermbg=NONE ctermfg=175 cterm=NONE guibg=NONE guifg=#d787af gui=NONE

elseif &t_Co == 8 || $TERM !~# '^linux' || &t_Co == 16
    set t_Co=16

    hi Delimiter ctermbg=NONE ctermfg=lightgrey cterm=NONE
    hi Normal ctermbg=black ctermfg=lightgrey cterm=NONE
    hi NonText ctermbg=NONE ctermfg=darkgrey cterm=NONE
    hi EndOfBuffer ctermbg=NONE ctermfg=darkgrey cterm=NONE
    hi LineNr ctermbg=black ctermfg=darkgrey cterm=NONE
    hi FoldColumn ctermbg=black ctermfg=lightgrey cterm=NONE
    hi Folded ctermbg=black ctermfg=lightgrey cterm=NONE
    hi MatchParen ctermbg=darkgrey ctermfg=yellow cterm=NONE
    hi SignColumn ctermbg=black ctermfg=lightgrey cterm=NONE
    hi Comment ctermbg=NONE ctermfg=darkgreen cterm=NONE
    hi Conceal ctermbg=NONE ctermfg=lightgrey cterm=NONE
    hi Constant ctermbg=NONE ctermfg=red cterm=NONE
    hi Error ctermbg=NONE ctermfg=darkred cterm=reverse
    hi Identifier ctermbg=NONE ctermfg=blue cterm=NONE
    hi Ignore ctermbg=NONE ctermfg=NONE cterm=NONE
    hi PreProc ctermbg=NONE ctermfg=magenta cterm=NONE
    hi Special ctermbg=NONE ctermfg=yellow cterm=NONE
    hi Statement ctermbg=NONE ctermfg=blue cterm=NONE
    hi String ctermbg=NONE ctermfg=red cterm=NONE
    hi Todo ctermbg=NONE ctermfg=yellow cterm=reverse
    hi Type ctermbg=NONE ctermfg=darkmagenta cterm=NONE
    hi Underlined ctermbg=NONE ctermfg=darkcyan cterm=underline
    hi Pmenu ctermbg=darkgrey ctermfg=lightgrey cterm=NONE
    hi PmenuSbar ctermbg=darkgrey ctermfg=NONE cterm=NONE
    hi PmenuSel ctermbg=blue ctermfg=white cterm=NONE
    hi PmenuThumb ctermbg=darkcyan ctermfg=darkcyan cterm=NONE
    hi ErrorMsg ctermbg=black ctermfg=darkred cterm=reverse
    hi ModeMsg ctermbg=black ctermfg=darkgreen cterm=reverse
    hi MoreMsg ctermbg=NONE ctermfg=darkcyan cterm=NONE
    hi Question ctermbg=NONE ctermfg=darkgreen cterm=NONE
    hi WarningMsg ctermbg=NONE ctermfg=darkred cterm=NONE
    hi TabLine ctermbg=darkgrey ctermfg=blue cterm=NONE
    hi TabLineFill ctermbg=darkgrey ctermfg=darkgrey cterm=NONE
    hi TabLineSel ctermbg=blue ctermfg=black cterm=NONE
    hi ToolbarLine ctermbg=black ctermfg=NONE cterm=NONE
    hi ToolbarButton ctermbg=darkgrey ctermfg=lightgrey cterm=NONE
    hi Cursor ctermbg=lightgrey ctermfg=NONE cterm=NONE
    hi CursorColumn ctermbg=black ctermfg=NONE cterm=NONE
    hi CursorLineNr ctermbg=black ctermfg=darkcyan cterm=NONE
    hi CursorLine ctermbg=black ctermfg=NONE cterm=NONE
    hi helpLeadBlank ctermbg=NONE ctermfg=NONE cterm=NONE
    hi helpNormal ctermbg=NONE ctermfg=NONE cterm=NONE
    hi StatusLine ctermbg=darkgrey ctermfg=white cterm=NONE
    hi StatusLineNC ctermbg=darkgrey ctermfg=blue cterm=NONE
    hi StatusLineTerm ctermbg=blue ctermfg=black cterm=NONE
    hi StatusLineTermNC ctermbg=darkgrey ctermfg=blue cterm=NONE
    hi Visual ctermbg=white ctermfg=blue cterm=reverse
    hi VisualNOS ctermbg=NONE ctermfg=NONE cterm=underline
    hi VertSplit ctermbg=darkgrey ctermfg=darkgrey cterm=NONE
    hi WildMenu ctermbg=blue ctermfg=white cterm=NONE
    hi Function ctermbg=NONE ctermfg=yellow cterm=NONE
    hi SpecialKey ctermbg=NONE ctermfg=darkgrey cterm=NONE
    hi Title ctermbg=NONE ctermfg=white cterm=NONE
    hi DiffAdd ctermbg=black ctermfg=darkgreen cterm=reverse
    hi DiffChange ctermbg=black ctermfg=darkmagenta cterm=reverse
    hi DiffDelete ctermbg=black ctermfg=darkred cterm=reverse
    hi DiffText ctermbg=black ctermfg=red cterm=reverse
    hi IncSearch ctermbg=yellow ctermfg=black cterm=NONE
    hi Search ctermbg=blue ctermfg=white cterm=NONE
    hi Directory ctermbg=NONE ctermfg=darkcyan cterm=NONE
    hi debugPC ctermbg=blue ctermfg=NONE cterm=NONE
    hi debugBreakpoint ctermbg=darkred ctermfg=NONE cterm=NONE
    hi SpellBad ctermbg=NONE ctermfg=darkred cterm=undercurl
    hi SpellCap ctermbg=NONE ctermfg=darkcyan cterm=undercurl
    hi SpellLocal ctermbg=NONE ctermfg=darkgreen cterm=undercurl
    hi SpellRare ctermbg=NONE ctermfg=red cterm=undercurl
    hi ColorColumn ctermbg=black ctermfg=NONE cterm=NONE
    hi DiffAdd ctermbg=black ctermfg=darkgreen cterm=NONE
    hi DiffChange ctermbg=black ctermfg=darkmagenta cterm=NONE
    hi DiffDelete ctermbg=black ctermfg=darkred cterm=NONE
    hi goRepeat ctermbg=NONE ctermfg=magenta cterm=NONE
endif

hi link Terminal Normal
hi link Number Constant
hi link CursorIM Cursor
hi link Boolean Constant
hi link Character Constant
hi link Conditional Statement
hi link Debug Special
hi link Define PreProc
hi link Exception Statement
hi link Float Number
hi link HelpCommand Statement
hi link HelpExample Statement
hi link Include PreProc
hi link Keyword Statement
hi link Label Statement
hi link Macro PreProc
hi link Number Constant
hi link Operator Special
hi link PreCondit PreProc
hi link Repeat Statement
hi link SpecialChar Special
hi link SpecialComment Special
hi link StorageClass Type
hi link Structure Type
hi link Tag Special
hi link Terminal Normal
hi link Typedef Type
hi link htmlTagName Statement
hi link htmlEndTag htmlTagName
hi link htmlLink Function
hi link htmlSpecialTagName htmlTagName
hi link htmlTag htmlTagName
hi link htmlBold Normal
hi link htmlItalic Normal
hi link htmlArg htmlTagName
hi link xmlTag Statement
hi link xmlTagName Statement
hi link xmlEndTag Statement
hi link markdownItalic Preproc
hi link asciidocQuotedEmphasized Preproc
hi link diffBDiffer WarningMsg
hi link diffCommon WarningMsg
hi link diffDiffer WarningMsg
hi link diffIdentical WarningMsg
hi link diffIsA WarningMsg
hi link diffNoEOL WarningMsg
hi link diffOnly WarningMsg
hi link diffRemoved WarningMsg
hi link diffAdded String
hi link QuickFixLine Search
hi link rustCommentLineDoc String
hi link terraBlockType Statement

let g:terminal_ansi_colors = [
        \ '#121212',
        \ '#d75f5f',
        \ '#5f875f',
        \ '#5fafd7',
        \ '#5fafd7',
        \ '#8787AF',
        \ '#5FAFAF',
        \ '#6c6c6c',
        \ '#444444',
        \ '#d7875f',
        \ '#5f875f',
        \ '#d7d787',
        \ '#5fafd7',
        \ '#8787AF',
        \ '#5FAFAF',
        \ '#DADADA',
        \ ]

" Generated with RNB (https://github.com/romainl/vim-rnb)
