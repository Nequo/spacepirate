![](assets/spacepirate.png)

# SpacePirate

This colorscheme is a rewrite of [vim-codedark](https://github.com/tomasiser/vim-code-dark/) using [vim-rnb](https://github.com/romainl/vim-rnb) with some personal tweaks and using colors from the xterm 256 palette.
The source code used as a base is a copy from romainl's great colorscheme [Apprentice](https://github.com/romainl/Apprentice).

## Installation

## Screenshots

## Xresources

```Xresources
! special
*.foreground:   #bcbcbc
*.background:   #1c1c1c
*.cursorColor:  #bcbcbc

! black
*.color0:       #262626
*.color8:       #585858

! red
*.color1:       #d75f5f
*.color9:       #d75f5f

! green
*.color2:       #5f875f
*.color10:      #5f875f

! yellow
*.color3:       #d7d787
*.color11:      #d7875f

! blue
*.color4:       #5fafd7
*.color12:      #005f87

! magenta
*.color5:       #8787af
*.color13:      #d75f87

! cyan
*.color6:       #5fafaf
*.color14:      #5fafaf

! white
*.color7:       #6c6c6c
*.color15:      #dadada
```
